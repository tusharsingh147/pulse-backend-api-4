package com.healthpulse.backend.api.persist.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "pulse_dev", name = "user_info")
public class UserInfoEntity extends BaseEntity{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String birthdate;
    private String aadhaar;
    private String healthIdNumber;
    private String gender;
    private String careOf;
    private String photo;
    private String email;
    private String phone;
    private String pin;
    private String house;
    private String street;
    private String landmark;
    private String villageTownCity;
    private String district;
    private String pincode;
    private String state;
}
