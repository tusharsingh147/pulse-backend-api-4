package com.healthpulse.backend.api.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class VerifyOtpResponse {

    private String token;

}
