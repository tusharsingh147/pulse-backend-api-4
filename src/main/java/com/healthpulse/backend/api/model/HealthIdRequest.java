package com.healthpulse.backend.api.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class HealthIdRequest {

    private String name;
    private String gender;
    private String yearOfBirth;
    private String txnId;
    private String token;

}
