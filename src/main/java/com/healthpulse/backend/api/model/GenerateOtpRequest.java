package com.healthpulse.backend.api.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GenerateOtpRequest {

    private String type;
    private String value;
}
